﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnDrivers = New System.Windows.Forms.Button()
        Me.btnEvents = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnRaceRecords = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.lblLogo = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnDrivers
        '
        Me.btnDrivers.Location = New System.Drawing.Point(12, 117)
        Me.btnDrivers.Name = "btnDrivers"
        Me.btnDrivers.Size = New System.Drawing.Size(121, 48)
        Me.btnDrivers.TabIndex = 0
        Me.btnDrivers.Text = "Drivers"
        Me.btnDrivers.UseVisualStyleBackColor = True
        '
        'btnEvents
        '
        Me.btnEvents.Location = New System.Drawing.Point(163, 117)
        Me.btnEvents.Name = "btnEvents"
        Me.btnEvents.Size = New System.Drawing.Size(121, 48)
        Me.btnEvents.TabIndex = 1
        Me.btnEvents.Text = "Events"
        Me.btnEvents.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(12, 194)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(121, 48)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'btnRaceRecords
        '
        Me.btnRaceRecords.Location = New System.Drawing.Point(320, 117)
        Me.btnRaceRecords.Name = "btnRaceRecords"
        Me.btnRaceRecords.Size = New System.Drawing.Size(121, 48)
        Me.btnRaceRecords.TabIndex = 3
        Me.btnRaceRecords.Text = "Results"
        Me.btnRaceRecords.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(163, 194)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(121, 48)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "Button5"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(320, 194)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(121, 48)
        Me.Button6.TabIndex = 5
        Me.Button6.Text = "Button6"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'lblLogo
        '
        Me.lblLogo.AutoSize = True
        Me.lblLogo.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLogo.Location = New System.Drawing.Point(96, 28)
        Me.lblLogo.Name = "lblLogo"
        Me.lblLogo.Size = New System.Drawing.Size(278, 39)
        Me.lblLogo.TabIndex = 6
        Me.lblLogo.Text = "GP West Motors"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(475, 349)
        Me.Controls.Add(Me.lblLogo)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.btnRaceRecords)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.btnEvents)
        Me.Controls.Add(Me.btnDrivers)
        Me.Name = "frmMain"
        Me.Text = "MainForm"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnDrivers As Button
    Friend WithEvents btnEvents As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents btnRaceRecords As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents lblLogo As Label
End Class
