﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEvents
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblGPEvents = New System.Windows.Forms.Label()
        Me.lblEventTitle = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.lblEventDate = New System.Windows.Forms.Label()
        Me.lblRegistrationFee = New System.Windows.Forms.Label()
        Me.lblEventLocation = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCloseEvent = New System.Windows.Forms.Button()
        Me.btnCancelEvent = New System.Windows.Forms.Button()
        Me.btnDeleteEvent = New System.Windows.Forms.Button()
        Me.btnSaveEvent = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblGPEvents
        '
        Me.lblGPEvents.AutoSize = True
        Me.lblGPEvents.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGPEvents.Location = New System.Drawing.Point(106, 19)
        Me.lblGPEvents.Name = "lblGPEvents"
        Me.lblGPEvents.Size = New System.Drawing.Size(170, 25)
        Me.lblGPEvents.TabIndex = 0
        Me.lblGPEvents.Text = "GP West Events"
        '
        'lblEventTitle
        '
        Me.lblEventTitle.AutoSize = True
        Me.lblEventTitle.Location = New System.Drawing.Point(40, 78)
        Me.lblEventTitle.Name = "lblEventTitle"
        Me.lblEventTitle.Size = New System.Drawing.Size(58, 13)
        Me.lblEventTitle.TabIndex = 1
        Me.lblEventTitle.Text = "Event Title"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(148, 72)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(209, 27)
        Me.TextBox1.TabIndex = 2
        '
        'lblEventDate
        '
        Me.lblEventDate.AutoSize = True
        Me.lblEventDate.Location = New System.Drawing.Point(39, 111)
        Me.lblEventDate.Name = "lblEventDate"
        Me.lblEventDate.Size = New System.Drawing.Size(61, 13)
        Me.lblEventDate.TabIndex = 3
        Me.lblEventDate.Text = "Event Date"
        '
        'lblRegistrationFee
        '
        Me.lblRegistrationFee.AutoSize = True
        Me.lblRegistrationFee.Location = New System.Drawing.Point(39, 144)
        Me.lblRegistrationFee.Name = "lblRegistrationFee"
        Me.lblRegistrationFee.Size = New System.Drawing.Size(84, 13)
        Me.lblRegistrationFee.TabIndex = 4
        Me.lblRegistrationFee.Text = "Registration Fee"
        '
        'lblEventLocation
        '
        Me.lblEventLocation.AutoSize = True
        Me.lblEventLocation.Location = New System.Drawing.Point(39, 178)
        Me.lblEventLocation.Name = "lblEventLocation"
        Me.lblEventLocation.Size = New System.Drawing.Size(79, 13)
        Me.lblEventLocation.TabIndex = 5
        Me.lblEventLocation.Text = "Event Location"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(148, 105)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(209, 27)
        Me.TextBox2.TabIndex = 6
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(148, 171)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(209, 29)
        Me.TextBox3.TabIndex = 7
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(148, 137)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(209, 29)
        Me.TextBox4.TabIndex = 8
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(148, 204)
        Me.TextBox5.Multiline = True
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(209, 29)
        Me.TextBox5.TabIndex = 9
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(39, 211)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(84, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Number Of Labs"
        '
        'btnCloseEvent
        '
        Me.btnCloseEvent.Location = New System.Drawing.Point(201, 260)
        Me.btnCloseEvent.Name = "btnCloseEvent"
        Me.btnCloseEvent.Size = New System.Drawing.Size(75, 23)
        Me.btnCloseEvent.TabIndex = 18
        Me.btnCloseEvent.Text = "Close"
        Me.btnCloseEvent.UseVisualStyleBackColor = True
        '
        'btnCancelEvent
        '
        Me.btnCancelEvent.Location = New System.Drawing.Point(121, 260)
        Me.btnCancelEvent.Name = "btnCancelEvent"
        Me.btnCancelEvent.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelEvent.TabIndex = 17
        Me.btnCancelEvent.Text = "Cancel"
        Me.btnCancelEvent.UseVisualStyleBackColor = True
        '
        'btnDeleteEvent
        '
        Me.btnDeleteEvent.Location = New System.Drawing.Point(282, 260)
        Me.btnDeleteEvent.Name = "btnDeleteEvent"
        Me.btnDeleteEvent.Size = New System.Drawing.Size(75, 23)
        Me.btnDeleteEvent.TabIndex = 16
        Me.btnDeleteEvent.Text = "Delete"
        Me.btnDeleteEvent.UseVisualStyleBackColor = True
        '
        'btnSaveEvent
        '
        Me.btnSaveEvent.Location = New System.Drawing.Point(40, 260)
        Me.btnSaveEvent.Name = "btnSaveEvent"
        Me.btnSaveEvent.Size = New System.Drawing.Size(75, 23)
        Me.btnSaveEvent.TabIndex = 15
        Me.btnSaveEvent.Text = "Save"
        Me.btnSaveEvent.UseVisualStyleBackColor = True
        '
        'frmEvents
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(393, 306)
        Me.Controls.Add(Me.btnCloseEvent)
        Me.Controls.Add(Me.btnCancelEvent)
        Me.Controls.Add(Me.btnDeleteEvent)
        Me.Controls.Add(Me.btnSaveEvent)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.lblEventLocation)
        Me.Controls.Add(Me.lblRegistrationFee)
        Me.Controls.Add(Me.lblEventDate)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.lblEventTitle)
        Me.Controls.Add(Me.lblGPEvents)
        Me.Name = "frmEvents"
        Me.Text = "Events"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblGPEvents As Label
    Friend WithEvents lblEventTitle As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents lblEventDate As Label
    Friend WithEvents lblRegistrationFee As Label
    Friend WithEvents lblEventLocation As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnCloseEvent As Button
    Friend WithEvents btnCancelEvent As Button
    Friend WithEvents btnDeleteEvent As Button
    Friend WithEvents btnSaveEvent As Button
End Class
