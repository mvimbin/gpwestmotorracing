﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDrivers
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblRacingDrivers = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtMembershipNumber = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblDateClubJoined = New System.Windows.Forms.Label()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.grpGender = New System.Windows.Forms.GroupBox()
        Me.rdbFemale = New System.Windows.Forms.RadioButton()
        Me.rdbMale = New System.Windows.Forms.RadioButton()
        Me.grbDriverOption = New System.Windows.Forms.GroupBox()
        Me.rdbEditDriver = New System.Windows.Forms.RadioButton()
        Me.rdbAddNewDriver = New System.Windows.Forms.RadioButton()
        Me.btnSaveDriver = New System.Windows.Forms.Button()
        Me.btnDeleteDriver = New System.Windows.Forms.Button()
        Me.btnCancelDriver = New System.Windows.Forms.Button()
        Me.btnCloseDriver = New System.Windows.Forms.Button()
        Me.grpGender.SuspendLayout()
        Me.grbDriverOption.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblRacingDrivers
        '
        Me.lblRacingDrivers.AutoSize = True
        Me.lblRacingDrivers.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRacingDrivers.Location = New System.Drawing.Point(86, 18)
        Me.lblRacingDrivers.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblRacingDrivers.Name = "lblRacingDrivers"
        Me.lblRacingDrivers.Size = New System.Drawing.Size(152, 25)
        Me.lblRacingDrivers.TabIndex = 0
        Me.lblRacingDrivers.Text = "Racing Drivers"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 119)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Membership Number"
        '
        'txtMembershipNumber
        '
        Me.txtMembershipNumber.Location = New System.Drawing.Point(139, 115)
        Me.txtMembershipNumber.Margin = New System.Windows.Forms.Padding(2)
        Me.txtMembershipNumber.Multiline = True
        Me.txtMembershipNumber.Name = "txtMembershipNumber"
        Me.txtMembershipNumber.Size = New System.Drawing.Size(189, 25)
        Me.txtMembershipNumber.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(11, 156)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Name and Surname"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(11, 186)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Birth Date"
        '
        'lblDateClubJoined
        '
        Me.lblDateClubJoined.AutoSize = True
        Me.lblDateClubJoined.Location = New System.Drawing.Point(9, 282)
        Me.lblDateClubJoined.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblDateClubJoined.Name = "lblDateClubJoined"
        Me.lblDateClubJoined.Size = New System.Drawing.Size(64, 13)
        Me.lblDateClubJoined.TabIndex = 5
        Me.lblDateClubJoined.Text = "Date Joined"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(139, 150)
        Me.TextBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(189, 25)
        Me.TextBox2.TabIndex = 6
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(139, 186)
        Me.TextBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.TextBox3.Multiline = True
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(189, 25)
        Me.TextBox3.TabIndex = 7
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(139, 276)
        Me.TextBox4.Margin = New System.Windows.Forms.Padding(2)
        Me.TextBox4.Multiline = True
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(189, 25)
        Me.TextBox4.TabIndex = 8
        '
        'grpGender
        '
        Me.grpGender.Controls.Add(Me.rdbFemale)
        Me.grpGender.Controls.Add(Me.rdbMale)
        Me.grpGender.Location = New System.Drawing.Point(11, 219)
        Me.grpGender.Margin = New System.Windows.Forms.Padding(2)
        Me.grpGender.Name = "grpGender"
        Me.grpGender.Padding = New System.Windows.Forms.Padding(2)
        Me.grpGender.Size = New System.Drawing.Size(317, 47)
        Me.grpGender.TabIndex = 9
        Me.grpGender.TabStop = False
        Me.grpGender.Text = "Gender"
        '
        'rdbFemale
        '
        Me.rdbFemale.AutoSize = True
        Me.rdbFemale.Location = New System.Drawing.Point(161, 20)
        Me.rdbFemale.Margin = New System.Windows.Forms.Padding(2)
        Me.rdbFemale.Name = "rdbFemale"
        Me.rdbFemale.Size = New System.Drawing.Size(59, 17)
        Me.rdbFemale.TabIndex = 1
        Me.rdbFemale.TabStop = True
        Me.rdbFemale.Text = "Female"
        Me.rdbFemale.UseVisualStyleBackColor = True
        '
        'rdbMale
        '
        Me.rdbMale.AutoSize = True
        Me.rdbMale.Location = New System.Drawing.Point(51, 20)
        Me.rdbMale.Margin = New System.Windows.Forms.Padding(2)
        Me.rdbMale.Name = "rdbMale"
        Me.rdbMale.Size = New System.Drawing.Size(48, 17)
        Me.rdbMale.TabIndex = 0
        Me.rdbMale.TabStop = True
        Me.rdbMale.Text = "Male"
        Me.rdbMale.UseVisualStyleBackColor = True
        '
        'grbDriverOption
        '
        Me.grbDriverOption.Controls.Add(Me.rdbEditDriver)
        Me.grbDriverOption.Controls.Add(Me.rdbAddNewDriver)
        Me.grbDriverOption.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbDriverOption.Location = New System.Drawing.Point(14, 44)
        Me.grbDriverOption.Margin = New System.Windows.Forms.Padding(2)
        Me.grbDriverOption.Name = "grbDriverOption"
        Me.grbDriverOption.Padding = New System.Windows.Forms.Padding(2)
        Me.grbDriverOption.Size = New System.Drawing.Size(314, 63)
        Me.grbDriverOption.TabIndex = 10
        Me.grbDriverOption.TabStop = False
        Me.grbDriverOption.Text = "Option"
        '
        'rdbEditDriver
        '
        Me.rdbEditDriver.AutoSize = True
        Me.rdbEditDriver.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbEditDriver.Location = New System.Drawing.Point(146, 25)
        Me.rdbEditDriver.Margin = New System.Windows.Forms.Padding(2)
        Me.rdbEditDriver.Name = "rdbEditDriver"
        Me.rdbEditDriver.Size = New System.Drawing.Size(133, 17)
        Me.rdbEditDriver.TabIndex = 1
        Me.rdbEditDriver.TabStop = True
        Me.rdbEditDriver.Text = "Edit Existing Driver"
        Me.rdbEditDriver.UseVisualStyleBackColor = True
        '
        'rdbAddNewDriver
        '
        Me.rdbAddNewDriver.AutoSize = True
        Me.rdbAddNewDriver.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.rdbAddNewDriver.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdbAddNewDriver.Location = New System.Drawing.Point(18, 25)
        Me.rdbAddNewDriver.Margin = New System.Windows.Forms.Padding(2)
        Me.rdbAddNewDriver.Name = "rdbAddNewDriver"
        Me.rdbAddNewDriver.Size = New System.Drawing.Size(112, 17)
        Me.rdbAddNewDriver.TabIndex = 0
        Me.rdbAddNewDriver.TabStop = True
        Me.rdbAddNewDriver.Text = "Add new Driver"
        Me.rdbAddNewDriver.UseVisualStyleBackColor = True
        '
        'btnSaveDriver
        '
        Me.btnSaveDriver.Location = New System.Drawing.Point(11, 321)
        Me.btnSaveDriver.Name = "btnSaveDriver"
        Me.btnSaveDriver.Size = New System.Drawing.Size(75, 23)
        Me.btnSaveDriver.TabIndex = 11
        Me.btnSaveDriver.Text = "Save"
        Me.btnSaveDriver.UseVisualStyleBackColor = True
        '
        'btnDeleteDriver
        '
        Me.btnDeleteDriver.Location = New System.Drawing.Point(253, 321)
        Me.btnDeleteDriver.Name = "btnDeleteDriver"
        Me.btnDeleteDriver.Size = New System.Drawing.Size(75, 23)
        Me.btnDeleteDriver.TabIndex = 12
        Me.btnDeleteDriver.Text = "Delete"
        Me.btnDeleteDriver.UseVisualStyleBackColor = True
        '
        'btnCancelDriver
        '
        Me.btnCancelDriver.Location = New System.Drawing.Point(92, 321)
        Me.btnCancelDriver.Name = "btnCancelDriver"
        Me.btnCancelDriver.Size = New System.Drawing.Size(75, 23)
        Me.btnCancelDriver.TabIndex = 13
        Me.btnCancelDriver.Text = "Cancel"
        Me.btnCancelDriver.UseVisualStyleBackColor = True
        '
        'btnCloseDriver
        '
        Me.btnCloseDriver.Location = New System.Drawing.Point(172, 321)
        Me.btnCloseDriver.Name = "btnCloseDriver"
        Me.btnCloseDriver.Size = New System.Drawing.Size(75, 23)
        Me.btnCloseDriver.TabIndex = 14
        Me.btnCloseDriver.Text = "Close"
        Me.btnCloseDriver.UseVisualStyleBackColor = True
        '
        'frmDrivers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(357, 368)
        Me.Controls.Add(Me.btnCloseDriver)
        Me.Controls.Add(Me.btnCancelDriver)
        Me.Controls.Add(Me.btnDeleteDriver)
        Me.Controls.Add(Me.btnSaveDriver)
        Me.Controls.Add(Me.grbDriverOption)
        Me.Controls.Add(Me.grpGender)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.lblDateClubJoined)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtMembershipNumber)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblRacingDrivers)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmDrivers"
        Me.Text = "Drivers"
        Me.grpGender.ResumeLayout(False)
        Me.grpGender.PerformLayout()
        Me.grbDriverOption.ResumeLayout(False)
        Me.grbDriverOption.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblRacingDrivers As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents txtMembershipNumber As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents lblDateClubJoined As Label
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents grpGender As GroupBox
    Friend WithEvents rdbFemale As RadioButton
    Friend WithEvents rdbMale As RadioButton
    Friend WithEvents grbDriverOption As GroupBox
    Friend WithEvents rdbEditDriver As RadioButton
    Friend WithEvents rdbAddNewDriver As RadioButton
    Friend WithEvents btnSaveDriver As Button
    Friend WithEvents btnDeleteDriver As Button
    Friend WithEvents btnCancelDriver As Button
    Friend WithEvents btnCloseDriver As Button
End Class
